def read_file(filename):
    '''reads the file and returns the list of cars in a dictionary
    as well as the header and footer information'''
    with open(filename) as f:
        header_footer = []
        cars = {}
        for line in f:
    	    if line[:2] in ['1\t', '2\t']:
                line = line.split()
	        get_cars(cars, line)
            elif line != '\n':
	        header_footer.append(line)
    return cars, header_footer

def get_cars(cars, line):
    '''gets the info on a car passing through

    in the form of plate:[time1, time2, speed]
    if any of the detected doesn't existed, the value is replaced by ''
    if speed does not exist, the value is replaced by 'speed unknown' '''
    detector, time, plate = line
    if plate not in cars:
	cars[plate] = ['', '']
    cars[plate][int(detector) - 1] = time
	

def get_speeds(cars):
    '''gets the speed of each car, if applicable'''
    for car, values in cars.items():
        time1, time2 = values
        duration = get_time_interval(time1, time2)
        if duration != 0:
	    values.append(float(200 / duration * 3.6)) # in km/h
        else:
	    values.append('speed unknown')



def get_time_interval(time1, time2):
    '''a helper function that calculates the time interval'''
    if time1 == '' or time2 == '':
	return 0
    else:
	result = convert_to_sec(time2) - convert_to_sec(time1)
	return result

def convert_to_sec(time):
    '''a helper function that convers time to seconds'''
    hour, minute, second = time.rsplit(':')
    result = int(hour) * 3600 + int(minute) * 60 + int(second)
    return result

def print_info(cars, header_footer):
    '''prints all relevant information'''
    print 'Speeding cars:\n%8s%8s%10s' % ('Plate', 'Speed', 'Time')
    for car, values in cars.items():
	if values[2] > 50 and values[2] != 'speed unknown':
	    print '%8s%8s%10s' % (car, values[2], values[1])
    
    interval_start = header_footer[0].rsplit(' ')[-1]
    interval_end = header_footer[1].rsplit(' ')[-1]
    print '\nTime interval of measurement: %s -- %s' \
	    % (interval_start[:-1], interval_end[:-1])
    
    speeds = [values[2] for key, values in cars.items() if values[2] != 'speed unknown']
    avg = sum(speeds) / len(speeds)
    print '\nAverage speed: %.2f km/h' % avg

    line0 = header_footer[0].split()
    date = {}
    date['year'], date['month'], date['day'] = line0[0:3]
    print '\nDate of measurement: %s-%s-%s' \
            % (date['day'], date['month'], date['year'])

    cars_between = [car for car, values in cars.items() if values[1] == '']
    print '\nCars between detectors:'
    for car in cars_between:
	    print car


INPUT_FILE = 'verkeer.txt'
cars, header_footer = read_file(INPUT_FILE)
get_speeds(cars)
print_info(cars, header_footer)


