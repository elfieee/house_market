
class Traffic():
    '''a class that manages traffic'''

    INPUT_FILE = 'verkeer.txt'

    def __init__(self):
        # self.cars in the form of plate:[time1, time2, speed]
        # if any of the detected doesn't existed, the value is replaced by ''
        # if speed does not exist, the value is replaced by 'speed unknown'
        self.cars = dict()
        self.date = dict()
        self.first_line, self.last_line = ['', '']

        self.read_file()
        self.get_speeds()

    def read_file(self):
        '''reads the input file line by line, and updates the attributes
        self.cars, self.date, self.first_line and self.last_line'''
        with open(Traffic.INPUT_FILE) as f:
            header_footer = []
            for line in f:
                if line[:2] in ['1\t', '2\t']:
                    line = line.split()
                    self.get_cars(line)
                elif line != '\n':
                    header_footer.append(line)
            #if.seek(0)
            #first_line = f.readline().rsplit(' ')
            self.first_line = header_footer[0]
            self.last_line = header_footer[-1]
            self.get_date()

    def get_date(self):
        '''updates attribute self.date'''
        line0 = self.first_line.split()
        self.date['year'], self.date['month'], self.date['day'] = line0[0:3]
    
    def get_cars(self, line):
        '''updates attribute self.cars (only time1 and time2)

        in the form of plate:[time1, time2, speed]
        if any of the detected doesn't existed, the value is replaced by ''
        if speed does not exist, the value is replaced by 'speed unknown' '''
        detector, time, plate = line
        if plate not in self.cars:
            self.cars[plate] = ['', '']
        if detector == '1':
            self.cars[plate][0] = time
        elif detector == '2':
            self.cars[plate][1] = time

    def get_speeds(self):
        '''updates attribute self.cars (speed)'''
        for car, values in self.cars.items():
            time1, time2 = values
            duration = self.get_time_interval(time1, time2)
            if duration != 0:
                values.append(float(200 / duration * 3.6)) # in km/h
            else:
                values.append('speed unknown')


    def get_time_interval(self, time1, time2):
        '''a helper function that calculates the time interval'''
        if time1 == '' or time2 == '':
            return 0
        else:
            result = self.convert_to_sec(time2) - self.convert_to_sec(time1)
            return result

    def convert_to_sec(self, time):
        '''a helper function that convers time to seconds'''
        hour, minute, second = time.rsplit(':')
        result = int(hour) * 3600 + int(minute) * 60 + int(second)
        return result

    def print_info(self):
        '''a method that prints all relevant information'''
        print 'Speeding cars:\n%8s%8s%10s' % ('Plate', 'Speed', 'Time')
        for car, values in self.cars.items():
            if values[2] > 50 and values[2] != 'speed unknown':
                print '%8s%8s%10s' % (car, values[2], values[1])
    
        interval_start = self.first_line.rsplit(' ')[-1]
        interval_end = self.last_line.rsplit(' ')[-1]
        print '\nTime interval of measurement: %s -- %s' \
                % (interval_start[:-1], interval_end[:-1])
    
        speeds = [values[2] for key, values in self.cars.items() if values[2] != 'speed unknown']
        avg = sum(speeds) / len(speeds)
        print '\nAverage speed: %.2f km/h' % avg

        print '\nDate of measurement: %s-%s-%s' \
                % (self.date['day'], self.date['month'], self.date['year'])

        cars_between = [car for car, values in self.cars.items() if values[1] == '']
        print '\nCars between detectors:'
        for car in cars_between:
            print car


if __name__ == '__main__':
    traffic = Traffic()
    traffic.print_info()
