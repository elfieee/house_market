import ipy_lib
ui = ipy_lib.HouseMarketUserInterface()


def get_file_content(file_name):
    '''reads a file containting lines in the form of <size>;<price><newline>

    input -> str
    output -> tuples: sizes, prices'''
    house_info = open(file_name, 'r')
    size_price_pair = [get_info(line) for line in house_info]
    sizes, prices = zip(*size_price_pair)
    return sizes, prices

def get_info(size_price):
    '''helper function for get_file_content()
    gets info on size and price from a <size>;<price><newline> string

    input -> str: <size>;<price><newline>
    output -> floats: size, price'''
    info = size_price.rsplit(';')
    size = float(info[0])
    price = float(info[1])
    return size, price

def get_coefficients(sizes, prices):
    '''gets the coefficients for linear regression on size-price'''
    slope = cov(sizes, prices) / cov(sizes, sizes)
    intercept = avg(prices) - slope * avg(sizes)
    return slope, intercept

def cov(x, y):
    '''gets COV(X, Y); also COV(X,X) = VAR(X)'''
    xy = tuple(map(lambda num1, num2: num1 * num2, x, y))
    covariance = avg(xy) - avg(x) * avg(y)
    return covariance

def avg(x):
    '''gets the average of the list of numbers in x'''
    result = float(sum(x) / len(x)) if len(x) > 0 else 'nan'
    return result

def draw_graphs(file_sold, file_for_sale):
    '''draws a graph of prices-sizes (indots) and its linear best fit
    also displays the price-size relation of houses for sale'''
    sizes_sold, prices_sold = get_file_content(file_sold)
    slope, intercept = get_coefficients(sizes_sold, prices_sold)
    sizes_for_sale, prices_for_sale = get_file_content(file_for_sale)
    # prints the data on houses sold
    for count in range(len(sizes_sold)):
        ui.plot_dot(sizes_sold[count], prices_sold[count], 'b')
    ui.plot_line(intercept, slope)

    # prints the data on houses for sale
    # also determines how many houses of these are affordable/expensive
    affordable, expensive = 0, 0
    for count in range(len(sizes_for_sale)):
        ui.plot_dot(sizes_for_sale[count], prices_for_sale[count], 'r')
        if slope * sizes_for_sale[count] < prices_for_sale[count]:
            expensive += 1
        else:
            affordable += 1
    ui.show()
    
    return expensive, affordable


    
if __name__ == '__main__':
    expensive, affordable = draw_graphs('houses_sold.txt',\
                                        'houses_for_sale.txt')
    if expensive > affordable:
        print 'The houses are expensive.'
    else:
        print 'The houses are affordable.'



